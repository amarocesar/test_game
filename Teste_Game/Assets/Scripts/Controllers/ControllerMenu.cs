﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControllerMenu : MonoBehaviour {

    public void PlayAgain()
    {
        SceneManager.LoadScene(1);
    }
    public void CloseTutorial(GameObject tutorial)
    {
        tutorial.SetActive(false);
    }
    public void OpenTutorial(GameObject tutorial)
    {
        tutorial.SetActive(true);
    }
}
