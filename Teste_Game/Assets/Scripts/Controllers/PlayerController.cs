﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    private Commands playerCommands;
    [HideInInspector]
    public Player player;
    public Text money;
    public Slider lifeSlider;
    float distToGround;
    Rigidbody2D rb;
    private bool directionRight;
    public GameObject panelInteract;
    int damage = 4;
    bool aliance = false;
    public GameObject gun;
    public GameObject painelInteract;
    public GameObject Fire;

    public void Start()
    {
        directionRight = true;
        
        rb = GetComponent<Rigidbody2D>();
        playerCommands = new MovePlayer();
        player = new Player();
        player.Life = 100;
        player.Itens = new List<GameObject>(4);
        player.Money = 0;
        lifeSlider.value = player.Life;
        player.Velocity = 200f;
        player.GetItem1 = false;
        player.GetItem2 = false;
        player.GetItem3 = false;
        player.CheckWin = false;
        player.GetWepon = false;
        money.text = player.Money.ToString();

        //para controlar de form simples os paineis de 
        //interação criei o painelInteract para ter acesso a todods os paineis
        painelInteract.transform.GetChild(0).gameObject.SetActive(true);
        Time.timeScale = 0;

    }

    public void ClosePanel(GameObject panel) {
        Time.timeScale = 1;
        panel.SetActive(false);
    }

    public void FireGun() {
        if (gun.activeSelf) {
            Instantiate(Fire, gun.transform.position,transform.rotation);
        }

    }


    public void Update()
    {
        
        if (Input.GetKey(KeyCode.LeftArrow)) {
            playerCommands.Rotate(this.transform, Vector3.forward, -player.Velocity);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            playerCommands.Rotate(this.transform, Vector3.forward, player.Velocity);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            playerCommands.Propursor(rb, player.Velocity);  
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            FireGun();
        }
    }

    public void FixedUpdate()
    {
        lifeSlider.value = player.Life;
        money.text = player.Money.ToString();
        if (player.Life < 0) {
            GameOver();
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.transform.CompareTag("asteroid"))
        {
            TakeDamage(1);
        }
        if (collision.transform.CompareTag("comet"))
        {
            TakeDamage(2);
        }
        if (collision.transform.CompareTag("sun"))
        {
            GameOver();
        }
        if (collision.transform.CompareTag("enemy"))
        {
            TakeDamage(20);
            Destroy(collision.gameObject);
        }
    }

    private void GameOver() {
        Time.timeScale = 0;
        painelInteract.transform.GetChild(4).gameObject.SetActive(true);
    }
    
    //Estabilizar movimentação ao colidir com cometas e asteroides
    private void OnCollisionExit2D(Collision2D collision)
    {
        rb.freezeRotation = true;
        rb.freezeRotation = false;
        rb.velocity = Vector2.zero;
        
    }

    //Interação para conseguir a arma 
    public void MakeAliance(GameObject panel) {
        player.GetWepon = true;
        gun.SetActive(true);
        IncertItem(gun);
        Time.timeScale = 1;
        panel.SetActive(false);
    }

    //Não aceita aliança  
    public void DeclineAliance(GameObject panel)
    {
        Time.timeScale = 1;
        panel.SetActive(false);
    }
    

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("aliance"))
        {
            panelInteract.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space)) {
                if (!gun.activeSelf)
                {
                    painelInteract.transform.GetChild(3).gameObject.SetActive(true);
                    Time.timeScale = 0;
                }
            }

        }
        if (collision.CompareTag("home"))
        {
            panelInteract.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (player.GetItem1 && player.GetItem2 && player.GetItem2)
                    player.CheckWin = true;
                else {
                    painelInteract.transform.GetChild(1).gameObject.SetActive(true);
                    Time.timeScale = 0;
                }

            }
        }
        if (collision.CompareTag("Item"))
        {
            panelInteract.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                player.GetItem1 = true;
                player.Money += 100;
                IsoleteItens(collision.gameObject);
            }
        }
        if (collision.CompareTag("Item1"))
        {
            panelInteract.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                player.GetItem2 = true;
                player.Money += 100;
                IsoleteItens(collision.gameObject);
            }
            
        }
        if (collision.CompareTag("Item2"))
        {
            panelInteract.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                player.GetItem3 = true;
                player.Money += 100;
                IsoleteItens(collision.gameObject);
            }

        }
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        panelInteract.SetActive(false);
    }


    public void MakeOrLoseMoney(float money)
    {
        player.Money += money;
    }

    //Deixar itens fora da visão do jogador
    public void IsoleteItens(GameObject item) {
        IncertItem(item);
        item.transform.position = new Vector3(1000, 1000, 1000);
    }
    // inserir item capturado no array
    public void  IncertItem(GameObject item) {
        if (!player.Itens.Contains(item)) {
            player.Itens.Add(item);
        }

       
    }

    public void TakeDamage(float multiplicador)
    {

        if (Mathf.Abs(rb.velocity.y) > 0.5f || Mathf.Abs(rb.velocity.x) > 0.5f)
        {
            player.Life -= Mathf.Abs(rb.velocity.y) + Mathf.Abs(rb.velocity.x) + (damage * multiplicador);
        }
    }

   
    
}
