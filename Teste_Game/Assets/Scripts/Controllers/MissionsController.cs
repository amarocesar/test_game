﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MissionsController : MonoBehaviour {
    PlayerController playerController;
    Player player;
    GameObject HotBar;
    public GameObject PanelWin;
    // Use this for initialization
    void Start () {
        HotBar = gameObject;
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        player = playerController.player;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetKeyUp(KeyCode.Space)) {
            CheckMission();
        }
	}

    public void PlayAgain() {
        SceneManager.LoadScene(1);
    }
    public void GoMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void EquipWeapon() {
        if (player.GetWepon)
        {
            PreencherHotBar();
        }
    }

    private void CheckWin()
    {
        if (player.CheckWin) {
            PanelWin.SetActive(true);
            Time.timeScale = 0;
        }
        
    }

    private void PreencherHotBar()
    {
        int count = 0;
        foreach (GameObject item in player.Itens) {
            Transform card = HotBar.transform.GetChild(count);
            Transform itemCard = card.GetChild(0);
            Image itemImage = itemCard.GetComponent<Image>();
            itemImage.color = item.GetComponent<SpriteRenderer>().color;
            itemImage.sprite = item.GetComponent<SpriteRenderer>().sprite;
            count += 1;
        }
    }

    private void CheckMission() {
        if (player.CheckWin)
        {
            CheckWin();
        }
        if (player.GetItem1) {
            PreencherHotBar();
        }

        if (player.GetItem2)
        {
            PreencherHotBar();
        }

        if (player.GetItem3)
        {
            PreencherHotBar();
        }
    }
}
