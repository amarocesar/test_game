﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerFire : MonoBehaviour {

    // Use this for initialization
    PlayerController playerController;
    private void Start()
    {
        Destroy(gameObject, 10);
    }
    void Update () {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * 30);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("enemy"))
        {
            playerController.MakeOrLoseMoney(30);
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }

}
