﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Commands
{
    //Move the player
    public abstract void Rotate(Transform playe, Vector3 direction, float velocity);
    public virtual void Propursor(Rigidbody2D player, float forceJump)
    {
        player.AddRelativeForce(Vector2.up * forceJump);
    }
}

public class MovePlayer : Commands
{

    public override void Rotate(Transform playe, Vector3 direction, float velocity)
    {
        playe.Rotate(direction*Time.deltaTime* velocity);
    }
}





