﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {
    private float life;
    private float money;
    private List<GameObject> itens;
    private float velocity;
    private bool getItem1, getItem2, getItem3, getWepon, checkWin;


    public float Life
    {
        get
        {
            return life;
        }

        set
        {
            life = value;
        }
    }

    public float Money
    {
        get
        {
            return money;
        }

        set
        {
            money = value;
        }
    }


    public float Velocity
    {
        get
        {
            return velocity;
        }

        set
        {
            velocity = value;
        }
    }

    public List<GameObject> Itens
    {
        get
        {
            return itens;
        }

        set
        {
            itens = value;
        }
    }

    public bool GetItem1
    {
        get
        {
            return getItem1;
        }

        set
        {
            getItem1 = value;
        }
    }

    public bool GetItem2
    {
        get
        {
            return getItem2;
        }

        set
        {
            getItem2 = value;
        }
    }

    public bool GetItem3
    {
        get
        {
            return getItem3;
        }

        set
        {
            getItem3 = value;
        }
    }

    public bool GetWepon
    {
        get
        {
            return getWepon;
        }

        set
        {
            getWepon = value;
        }
    }

    public bool CheckWin
    {
        get
        {
            return checkWin;
        }

        set
        {
            checkWin = value;
        }
    }
}
