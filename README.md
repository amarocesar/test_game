# README #
1- Short README about the project and the assumptions about storyline and spec

One of the first considerations about this project concerns a mistaken 
decision-making, since I started a game with an extensive scope, but due 
to the limit of working time, I had to go back on my decision. Due to lack 
of time, panel texts were added directly to the panels rather than in a Json 
File for a single panel. However, my "planets in trouble" project, 
which came about because of this project, it will be completed shortly. 
So, instead of this game, I chose to develop "protecting the earth" because 
it is a simple project that facilitates the production of the required features, 
it is a 2D action game, of control of spaceships, in which the purpose is to 
capture items and bring them to a specific point, with a spaceship the player 
must capture the items for your planet, but to achieve such a feat, whoever 
plays must destroy enemies and dodge asteroids. 
Storyline :
The enemy aliens called "the Lords of the Noronha" are at war with other planets, they want to destroy the solar system and surround the solar system of asteroids. To save the inhabitants of the planet Butantan, the spaceship needs to collect three key pieces to build a weapon to defeat the enemy threat. 

________________________________________________________________________________

Uma das primeiras considera��es sobre este projeto refere-se a uma tomada 
de decis�o equivocada, pois iniciei um jogo com um escopo extenso, mas em decorr�ncia 
do limite de tempo de trabalho, tive que voltar atr�s na minha decis�o. Por 
falta de tempo, os textos dos pain�is foram adicionados diretamente � pain�is 
e n�o em um arquivo Json para um �nico painel. Por�m, meu projeto �planetas 
em apuros�, que surgiu em raz�o deste projeto, ser� conclu�do em breve. 
Assim, em vez deste jogo, optei por desenvolver �protegendo a terra�, pois 
� um projeto simples que facilita a produ��o das caracter�sticas exigidas, � 
um jogo 2D e de a��o, de controle de naves, em que o objetivo � capturar itens 
e traz�-los para um ponto espec�fico, com uma nave o jogador deve capturar os 
itens para o seu planeta, mas para conseguir tal feito, o jogador dever� destruir 
inimigos e esquivar-se de aster�ides. 
Hist�ria do jogo:
Os alien�genas inimigos chamados �os senhores do noronha� est�o em guerra com 
outros planetas, eles querem destruir o sistema solar e cercar o sistema solar 
de aster�ides. Para salvar os habitantes do planeta Butant�, a nave precisa 
coletar tr�s pe�as fundamentais para construir uma arma para derrotar a 
amea�a inimiga.  
